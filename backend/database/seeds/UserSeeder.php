<?php

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => Hash::make('password'),
            'email_verified_at' => Carbon::now(),
            'is_admin' => true
        ]);
        User::create([
            'name' => 'Fahmi',
            'email' => 'zulfahmi@gmail.com',
            'password' => Hash::make('password'),
            'email_verified_at' => Carbon::now(),
            'is_admin' => false
        ]);
    }
}
