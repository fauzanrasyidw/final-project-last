<?php

use App\Genre;
use Illuminate\Database\Seeder;

class GenreSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Genre::create(['genre_name' => 'Action']);
        Genre::create(['genre_name' => 'Mystery']);
        Genre::create(['genre_name' => 'Historical Fiction']);
        Genre::create(['genre_name' => 'Biography']);
        Genre::create(['genre_name' => 'Science']);
        Genre::create(['genre_name' => 'Art']);
        Genre::create(['genre_name' => 'Comic']);
        Genre::create(['genre_name' => 'Horror']);
        Genre::create(['genre_name' => 'Romance']);
    }
}
