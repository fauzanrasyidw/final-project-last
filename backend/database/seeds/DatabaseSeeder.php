<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // UserS
        $this->call(UserSeeder::class);
        $this->call(GenreSeeder::class);
        $this->call(BookSeeder::class);

    }
}
