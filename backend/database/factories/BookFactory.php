<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Book;
use App\Genre;
use Faker\Generator as Faker;

$factory->define(Book::class, function (Faker $faker) {
    return [
        'title' => $faker->sentence(3),
        'author' => $faker->firstName() . " " . $faker->lastName(),
        'year' => $faker->numberBetween(1950, 2020),
        'stock' => $faker->numberBetween(20, 50),
        'genre_id' => Genre::inRandomOrder()->first()->id

    ];
});
