<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
|
|
*/
Route::apiResource('/book', 'BookController');
Route::apiResource('/genre', 'GenreController');
Route::apiResource('/profile', 'ProfileController');
Route::apiResource('/review', 'ReviewController');
Route::apiResource('/borrow', 'BorrowController');
Route::apiResource('/profile','ProfileController');
// Route::post('/borrow/{id}/return', 'BookController@return');

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group([
    'prefix' => 'auth',
    'namespace' => 'AuthApi'
], function() {
    Route::post('register', 'RegisterController')->name('auth.register');
    Route::post('regenerate-otp-code', 'RegenerateOTPCodeController')->name('auth.regenerate');
    Route::post('verification', 'VerificationController')->name('auth.verification');
    Route::post('updatepassword', 'UpdatePasswordController')->name('auth.updatepassword');
    Route::post('login', 'LoginController')->name('auth.login');
});
// Route::post('auth/register', '');
