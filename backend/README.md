<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400"></a></p>

<p align="center">
<a href="https://travis-ci.org/laravel/framework"><img src="https://travis-ci.org/laravel/framework.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/d/total.svg" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/v/stable.svg" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://poser.pugx.org/laravel/framework/license.svg" alt="License"></a>
</p>

## Tim Kami

Anggota Tim:
1. Sahadat Islam islamsyahadad@gmail.com
2. Helmy Hidayat helmytahfizh@gmail.com
3. Fauzan Rasyid Winanto rasyidwinanto13@gmail.com

## Aplikasi Kami

Kami membuat aplikasi berbasis pinjam meminjam di perpustakaan, dengan fitur:
1. Registrasi untuk pengguna, menggunakan email yang kemudian dikirimkan OTP (sementara masih menggunakan Mailtrap), yang kode OTP tersebut harus diverifikasi agar email tersebut bisa diubah password nya, dan diaktifkan
2. User yang teregistrasi otomatis sebagai pengguna. Sedangkan admin (dicek dari is_admin == true) hanya bisa diubah di kode/phpmyadmin saja
3. Pengguna dapat melihat buku dan memberikan review buku, dan update profil masing-masing
4. Admin dapat melakukan semua yang dilakukan Pengguna ditambah dapat menambah/menghapus buku, dan melakukan transaksi peminjaman (menambah record peminjaman dan menghapus record peminjaman)

## API kami

Kami mengupload API Web Service kami di situs: pa.smaira.sch.id/api
dan dokumentasinya adalah di sini [Dokumentasi API](https://documenter.getpostman.com/view/18391537/UVR5po3q)

## Frontend kami

Kami masih banyak kesulitan dalam mengembangkan Vue, sehingga kami belum melakukan upload di hosting dan masih banyak fitur yang belum kami masukkan.

## Desain ERD
Rancangan ERD kami ada di folder backend/resource/views/Desain ERD.jpg

<img src="https://gitlab.com/fauzanrasyidw/final-project-last/-/raw/master/backend/resources/views/desain%20erd.jpg" alt="ERD">
