<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    protected $fillable = ['book_id', 'user_id', 'comment', 'star'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // 1 review untuk 1 buku
    public function book() {
        return $this->belongsTo('App\Book');
    }

    // 1 review dari 1 user
    public function user() {
        return $this->belongsTo('App\User');
    }
}
