<?php

namespace App\Http\Controllers;
use App\Book;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class BookController extends Controller
{
    public function __construct()
    {
       return $this->middleware('auth:api')->only(['store', 'update', 'delete' ]);
    }

    public function index()
    {
        //get data from table book
        $book = Book::latest()->with('genre')->get();
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Booku',
            'data'    => $book
        ], 200);
        /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    }
    public function show($id)
    {
        //find Booku by ID
        $book = Book::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data book$book',
            'data'    => $book
        ], 200);
    }
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $allrequest = $request->all();
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'author' => 'required',
            'year' => 'required',
            'stock' => 'required',
            'genre_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //save to database
        $book = Book::create([
            'title'     => $request->title,
            'author'   => $request->author,
            'year'   => $request->year,
            'stock'   => $request->stock,
            'genre_id'   => $request->genre_id,
        ]);
        //success save to database
        if ($book) {

            return response()->json([
                'success' => true,
                'message' => 'Book Created',
                'data'    => $book
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Book Failed to Save',
        ], 409);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $Booku
     * @return void
     */
    public function update(Request $request, Book $book)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'title'   => 'required',
            'author' => 'required',
            'year' => 'required',
            'stock' => 'required',
            'genre_id' => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find book by ID
        $book = Book::findOrFail($book->id);

        if ($book) {

            //update book
            $book->update([
                'title'     => $request->title,
                'author'   => $request->author,
                'year'   => $request->year,
                'stock'   => $request->stock,
                'genre_id'   => $request->genre_id,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'book Updated',
                'data'    => $book
            ], 200);
        }

        //data book not found
        return response()->json([
            'success' => false,
            'message' => 'Book Not Found',
        ], 404);
    }
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Book by ID
        $book = Book::findOrfail($id);

        if ($book) {

            //delete book
            $book->delete();

            return response()->json([
                'success' => true,
                'message' => 'book Deleted',
            ], 200);
        }

        //data book not found
        return response()->json([
            'success' => false,
            'message' => 'book Not Found',
        ], 404);
    }
}
