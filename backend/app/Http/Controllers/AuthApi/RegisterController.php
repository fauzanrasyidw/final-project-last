<?php

namespace App\Http\Controllers\AuthApi;

use App\Events\OtpCodeStoreEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'name' => 'required',
            'email' => 'unique:users,email|required|email'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        $user = User::create($allRequest);

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ( $check );

        $now = Carbon::now(); 

        $otp_code = OtpCode::create([
            'otp' => $random,
            'user_id' => $user->id,
            'valid_until' => $now->addMinutes(5),
        ]);

        event(new OtpCodeStoreEvent($otp_code, true));

        // kirim email OTP ke email yang registrasi

        return response()->json([
            'success' => true,
            'message' => 'data user telah dibuat',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 200);
    }
}
