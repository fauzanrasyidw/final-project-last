<?php

namespace App\Http\Controllers\AuthApi;

use App\Events\OtpCodeStoreEvent;
use App\Http\Controllers\Controller;
use App\OtpCode;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class RegenerateOTPCodeController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $allRequest = $request->all();

        $validator = Validator::make($allRequest, [
            'email' => 'required',
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $user = User::where('email', $request->email)->first();

        if($user->otp_code){
            $user->otp_code->delete();
        }

        do {
            $random = mt_rand(100000, 999999);
            $check = OtpCode::where('otp', $random)->first();
        } while ( $check );

        $now = Carbon::now();

        $otp_code = OtpCode::create([
            'otp' => $random,
            'user_id' => $user->id,
            'valid_until' => $now->addMinutes(5),
        ]);

        
        event(new OtpCodeStoreEvent($otp_code));


        // kirim email OTP ke email yang registrasi

        return response()->json([
            'success' => true,
            'message' => 'data OTP telah diregenerate',
            'data' => [
                'user' => $user,
                'otp_code' => $otp_code
            ]
        ], 200);
    }
}
