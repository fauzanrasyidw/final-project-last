<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Review;
use Illuminate\Http\Request;

class ReviewController extends Controller
{
    public function __construct()
    {
       return $this->middleware('auth:api')->only(['store', 'update', 'delete' ]);
    }

    public function index()
    {
        //get data from table book
        $review = Review::latest()->get();
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Review',
            'data'    => $review
        ], 200);
        /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    }
    public function show($id)
    {
        $review = Review::findOrfail($id);

        return response()->json([
            'success' => true,
            'message' => 'Detail Data Review',
            'data'    => $review
        ], 200);
    }
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $allrequest = $request->all();
        $validator = Validator::make($request->all(), [
            'book_id'   => 'required',
            'user_id' => 'required',
            'comment' => 'required',
            'star' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //save to database
        $review = Review::create([
            'book_id'     => $request->book_id,
            'user_id'   => $request->user_id,
            'comment'   => $request->comment,
            'star'   => $request->star
        ]);
        //success save to database
        if ($review) {

            return response()->json([
                'success' => true,
                'message' => 'Review Created',
                'data'    => $review
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Review Failed to Save',
        ], 409);
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $Booku
     * @return void
     */
    public function update(Request $request, Review $review)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'book_id'   => 'required',
            'user_id' => 'required',
            'comment' => 'required',
            'star' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        //find book by ID
        $review = Review::findOrFail($review->id);

        if ($review) {

            //update book
            $review->update([
                'book_id'     => $request->book_id,
                'user_id'   => $request->user_id,
                'comment'   => $request->comment,
                'star'   => $request->star
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Review Updated',
                'data'    => $review
            ], 200);
        }

        //data Review not found
        return response()->json([
            'success' => false,
            'message' => 'Review Not Found',
        ], 404);
    }
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        //find Book by ID
        $review = Review::findOrfail($id);

        if ($review) {

            //delete book
            $review->delete();

            return response()->json([
                'success' => true,
                'message' => 'Review Deleted',
            ], 200);
        }

        //data book not found
        return response()->json([
            'success' => false,
            'message' => 'book Not Found',
        ], 404);
    }
}
