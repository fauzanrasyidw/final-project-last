<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Validator;
use App\Profile;
use Illuminate\Http\Request;

class ProfileController extends Controller
{
    public function __construct()
    {
       return $this->middleware('auth:api')->only(['index','store', 'update', 'delete' ]);
    }

    public function index()
    {
        // Cek apakah user punya profil
        $profile = auth()->user()->profile;

        if ($profile) {
            return response()->json([
                'success' => true,
                'message' => 'List Data Profile',
                'data'    => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'This user has no profile'
        ], 400);
        /**
     * show
     *
     * @param  mixed $id
     * @return void
     */
    }
    // public function show($id)
    // {
    //     $profile = Profile::findOrfail($id);

    //     //make response JSON
    //     return response()->json([
    //         'success' => true,
    //         'message' => 'Detail Data Profile',
    //         'data'    => $profile
    //     ], 200);
    // }
    /**
     * store
     *
     * @param  mixed $request
     * @return void
     */
    public function store(Request $request)
    {
        //set validation
        $allreques = $request->all();
        $validator = Validator::make($request->all(), [
            'nickname'   => 'required',
            'phone'   => 'required',
            'address'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = auth()->user()->profile;
        // Cek apakah profil ada
        if ($profile)
        {
            return response()->json([
                'success' => false,
                'message' => 'This user already has profile',
            ], 409);
        }
        $profile = Profile::create([
            'nickname'     => $request->nickname,
            'phone'        => $request->phone,
            'address'      => $request->address,
            'user_id'      => auth()->user()->id
        ]);
        //success save to database
    }
    /**
     * update
     *
     * @param  mixed $request
     * @param  mixed $profile
     * @return void
     */
    public function update(Request $request, Profile $profile)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            'nickname'   => 'required',
            'phone'   => 'required',
            'address'   => 'required',
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $profile = auth()->user()->profile;

        if ($profile) {

            $profile->update([
            'nickname'     => $request->nickname,
            'phone'     => $request->phone,
            'address'     => $request->address,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Profile Updated',
                'data'    => $profile
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Profile Not Found',
        ], 404);
    }
    /**
     * destroy
     *
     * @param  mixed $id
     * @return void
     */
    public function destroy($id)
    {
        $profile = auth()->user()->profile;

        if ($profile) {

            $profile->delete();

            return response()->json([
                'success' => true,
                'message' => 'Profile Deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Profile Not Found',
        ], 404);
    }
}
