<?php

namespace App\Http\Controllers;

use App\BookBorrow;
use Illuminate\Support\Facades\Validator;
use App\Borrow;
use App\Genre;
use Illuminate\Http\Request;

class BorrowController extends Controller
{
    public function __construct()
    {
        return $this->middleware(['auth:api'])->only(['store', 'update', 'delete']);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrow = Borrow::latest()->get();
        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'List Data Borrowers',
            'data'    => $borrow->load('books')
        ], 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */


    public function store(Request $request)
    {
        $allrequest = $request->all();
        $validator = Validator::make($request->all(), [
            'borrow_at' => 'required|date',
            'peminjam_id' => 'required|exists:users,id',
            'books_id' => 'required'
        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }
        //save to database
        $borrow = Borrow::create([
            // 'id'     => $request->id,
            'borrow_at'     => $request->borrow_at,
            'receptionist_id'     => auth()->user()->id,
            'peminjam_id'     => $request->peminjam_id,
        ]);
        $borrow->books()->sync($request->books_id);
        //success save to database
        if ($borrow) {
            return response()->json([
                'success' => true,
                'message' => 'Borrowers Added',
                'data'    => $borrow
            ], 201);
        }

        //failed save to database
        return response()->json([
            'success' => false,
            'message' => 'Borrowers Failed to added',
        ], 409);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */



    public function show($id)
    {
        $borrow = Borrow::findOrfail($id);

        //make response JSON
        return response()->json([
            'success' => true,
            'message' => 'Detail Data Borrow',
            'data'    => $borrow
        ], 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */




    // Untuk kembalikan buku
    public function update(Request $request, $id)
    {
        //set validation
        $validator = Validator::make($request->all(), [
            // 'id'   => 'required',
            'back_at' => 'required|date',

        ]);

        //response error validation
        if ($validator->fails()) {
            return response()->json($validator->errors(), 400);
        }

        $borrow = Borrow::findOrFail($request->id);

        if ($borrow) {

            $borrow->update([
                // 'id'     => $request->id,
                'back_at'     => $request->back_at,
            ]);

            return response()->json([
                'success' => true,
                'message' => 'Books returned',
                'data'    => $borrow
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Borrowing record not found',
        ], 404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $borrow = Borrow::findOrfail($id);

        if ($borrow) {
            BookBorrow::where("borrow_id", $borrow->id)->delete();
            
            $borrow->delete();

            return response()->json([
                'success' => true,
                'message' => 'Borrowing Record deleted',
            ], 200);
        }

        return response()->json([
            'success' => false,
            'message' => 'Borrowing Record not Found',
        ], 404);
    }
    // pengembalian buku
    // public function return($id, Request $request)
    // {
    //     $validator = Validator::make($request->all(), [
    //         'back_at' => 'required|date',
    //     ]);
    //     $borrow = Borrow::findOrfail($id);

    //     if ($borrow) {

    //         $borrow->update([
    //             'back_at' => $request->back_at,
    //         ]);

    //         return response()->json([
    //             'success' => true,
    //             'message' => 'Books returned',
    //         ], 200);
    //     }

    //     return response()->json([
    //         'success' => false,
    //         'message' => 'Borrowing Record not Found',
    //     ], 404);
    // }
}
