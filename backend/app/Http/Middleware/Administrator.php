<?php

namespace App\Http\Middleware;

use Closure;

class Administrator
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // Cek apakah user tersebut adalah admin
        if ( !$request->user()->is_admin ) {
            return route('auth.login');
        }
        return $next($request);
    }
}
