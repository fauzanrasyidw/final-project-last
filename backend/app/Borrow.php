<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Borrow extends Model
{
    protected $guarded = ['id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {

        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    // 1 transaksi peminjaman punya 1 peminjam
    public function peminjam() {
        return $this->belongsTo('App\User', 'peminjam_id');
    }

    // 1 transaksi peminjaman punya 1 petugas
    public function petugas() {
        return $this->belongsTo('App\User', 'receptionist_id');
    }

    // 1 transaksi bisa meminjam banyak buku sekaligus
    public function books() {
        return $this->belongsToMany('App\Book', 'book_borrows'); //argumen kedua untuk nama tabel nya
    }
}
