<?php

namespace App;

use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = ['title', 'author', 'year', 'stock', 'genre_id'];
    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });

    }

    // 1 buku punya 1 Genre
    public function genre() {
        return $this->belongsTo('App\Genre');
    }

    // 1 buku punya banyak review, cuma panahnya menuju ke books jadi pake hasMany
    public function reviews() {
        return $this->hasMany('App\Review');
    }

    // 1 buku bisa dipinjam di banyak transaksi
    public function borrows() {
        return $this->belongsToMany('App\Borrow','book_borrows');
    }
}
