<?php

namespace App;

use App\OtpCode;
use App\Profile;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Str;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password' , 'email_verified_at'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    protected $primaryKey = 'id';
    protected $keyType = 'string';
    public $incrementing = false;

    protected static function boot()
    {

        parent::boot();

        static::creating(function ($model) {
            if (empty($model->{$model->getKeyName()})) {
                $model->{$model->getKeyName()} = Str::uuid();
            }
        });
    }

    public function otp_code()
    {
        return $this->hasOne('App\OtpCode');
    }

    // 1 user punya 1 profil
    public function profile() {
        return $this->hasOne('App\Profile');
    }

    // 1 user bisa punya banyak review
    public function reviews() {
        return $this->hasMany('App\Review');
    }

    // 1 user bisa meminjam di banyak transaksi borrow
    public function pinjam() {
        return $this->hasMany('App\Borrow', 'peminjam_id');
    }

    // 1 user bisa menjadi petugas di banyak transaksi borrow
    public function petugas() {
        return $this->hasMany('App\Borrow', 'receptionist_id');
    }

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

}
